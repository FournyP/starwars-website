<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
	<title>Login</title>
</head>
<body>
	<div class="container">
		<form method="POST">
			<div class="row">
				<div class="col-auto">
				    <label for="email" class="visually-hidden">Email</label>
				    <input type="text" class="form-control" id="email" name="email">
				  </div>
			</div>
			<div class="row">
				<div class="col-auto">
				    <label for="password" class="visually-hidden">Password</label>
		    		<input type="password" class="form-control" id="password" name="password">
				  </div>
			</div>
			<div class="row">
				<div class="col-auto">
				    <button type="submit" class="btn btn-primary mb-3">Login</button>
				  </div>
			</div>
		</form>
	</div>
</body>
</html>