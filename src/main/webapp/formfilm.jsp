<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
		<title>Insert title here</title>
	</head>
	<body>
		<div class="container">
			<form method="POST">
				<div class="row">
					<div class="col-auto">
					    <label for="title" class="visually-hidden">Title</label>
					    <input type="text" class="form-control" id="title" name="title" value="<%= request.getAttribute("title") != null ? request.getAttribute("title") : "" %>">
					    <small id="titleSmall" class="form-text text-muted"><%= request.getAttribute("title") != null ? request.getAttribute("title") : "" %></small>
					  </div>
				</div>
				<div class="row">
					<div class="col-auto">
					    <label for="release_year" class="visually-hidden">Release year</label>
			    		<input type="number" class="form-control" id="release_year" name="release_year" value="<%= request.getAttribute("release_year") != null ? request.getAttribute("release_year") : "" %>">
					  </div>
				</div>
				<div class="row">
					<div class="col-auto">
					    <label for="num_episode" class="visually-hidden">Number of episodes</label>
			    		<input type="number" class="form-control" id="num_episode" name="num_episode" value="<%= request.getAttribute("num_episode") != null ? request.getAttribute("num_episode") : "" %>">
					  </div>
				</div>
				<div class="row">
					<div class="col-auto">
					    <label for="cost" class="visually-hidden">Cost</label>
			    		<input type="number" class="form-control" id="cost" name="cost" value="<%= request.getAttribute("cost") != null ? request.getAttribute("cost") : "" %>">
					  </div>
				</div>
				<div class="row">
					<div class="col-auto">
					    <label for="receipt" class="visually-hidden">Receipt</label>
			    		<input type="number" class="form-control" id="receipt" name="receipt" value="<%= request.getAttribute("receipt") != null ? request.getAttribute("receipt") : "" %>">
					  </div>
				</div>
				<div class="row">
					<div class="col-auto">
						<button type="submit">Save</button>
					</div>
				</div>
			</form>
		</div>
	</body>
	<script>
		document.getElementById("title").addEventListener('keydown', function(event) {
			document.getElementById("titleSmall").innerHTML = document.getElementById("title").value;
		});
		document.getElementById("title").addEventListener('keyup', function(event) {
			document.getElementById("titleSmall").innerHTML = document.getElementById("title").value;
		});
	</script>
</html>