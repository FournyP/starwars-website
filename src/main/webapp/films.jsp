<%@ page import="modeles.Film" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
		<script src="https://kit.fontawesome.com/e439e531ea.js" crossorigin="anonymous"></script>
		<title>Films</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-4 mr-auto">
					<a class="btn btn-primary" href="/starwars/film/new">Ajouter un film</a>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<table class="table">
					  <thead>
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">Title</th>
					      <th scope="col">Release year</th>
					      <th scope="col">Cost</th>
					      <th scope="col">Receipt</th>
					      <th scope="col">Actions</th>
					    </tr>
					  </thead>
					  <tbody>
						<%
							for(Film film : (List<Film>) request.getAttribute("films"))
							{
								out.println("<tr>");
								out.println("<th scope='row'>" + film.getId() + "</th>");
								out.println("<td>" + film.getTitle() + "</td>");
								out.println("<td>" + film.getReleaseYear() + "</td>");
								out.println("<td>" + film.getCost() + "</td>");
								out.println("<td>" + film.getReceipt() + "</td>");
								out.println("<td class='align-middle'>");
								out.println("<a style='color:inherit;' href='/starwars/film/delete?id=" + film.getId() + "'><i class='fas fa-trash'></i></a>");
								out.println("<a style='color:inherit;' href='/starwars/film/update?id=" + film.getId() + "'><i class='fas fa-edit'></i></a>");
								out.println("</td>");
								out.println("</tr>");
							}
						%>					  	
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>