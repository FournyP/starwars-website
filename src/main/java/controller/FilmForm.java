package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DAOFilm;
import modeles.Acteur;
import modeles.Film;

/**
 * Servlet implementation class FilmForm
 */
@WebServlet(urlPatterns = {"/film/new", "/film/update"})
public class FilmForm extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private DAOFilm daoFilm;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FilmForm() {
        super();
        
        daoFilm = new DAOFilm("root", "1234", "127.0.0.1", "3306", "starwars");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("id") != null) {
			
			String idStr = request.getParameter("id");
			
			try {
			
				int id = Integer.parseInt(idStr);

				Film film = daoFilm.findOneById(id);
				
				if (film != null) {
					
					request.setAttribute("title", film.getTitle());
					request.setAttribute("release_year", film.getReleaseYear());
					request.setAttribute("num_episode", film.getNumEpisode());
					request.setAttribute("cost", film.getCost());
					request.setAttribute("receipt", film.getReceipt());
				}
				
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
		request.getRequestDispatcher("/formfilm.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String title = request.getParameter("title");
		String releaseYearStr = request.getParameter("release_year");
		String numEpisodeStr = request.getParameter("num_episode");
		String costStr = request.getParameter("cost");
		String receiptStr = request.getParameter("receipt");
		
		try {
			
			int numEpisode = Integer.parseInt(numEpisodeStr);
			int releaseYear = Integer.parseInt(releaseYearStr);
			int cost = Integer.parseInt(costStr);
			int receipt = Integer.parseInt(receiptStr);

			if (request.getParameter("id") != null) {
				
				String idStr = request.getParameter("id");
			
				try {
					
					int id = Integer.parseInt(idStr);

					Film film = daoFilm.findOneById(id);
					
					if (film != null) {
						
						film.setNumEpisode(numEpisode);
						film.setReleaseYear(releaseYear);
						film.setCost(cost);
						film.setReceipt(receipt);
						
						daoFilm.modifier(film);
					}
				} catch (NumberFormatException e) {
					doGet(request, response);
				}
				
			} else {
			
				Film film = new Film(title, releaseYear, numEpisode, (long) cost, (long) receipt, new ArrayList<Acteur>());
				
				daoFilm.ajouter(film);
			}
			
			response.sendRedirect("/starwars/films");
			
		} catch (NumberFormatException e) {
			doGet(request, response);
		}
	}

}
