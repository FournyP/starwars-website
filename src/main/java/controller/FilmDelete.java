package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DAOFilm;

/**
 * Servlet implementation class FilmDelete
 */
@WebServlet(urlPatterns = {"/film/delete"})
public class FilmDelete extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private DAOFilm daoFilm;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FilmDelete() {
        super();

        daoFilm = new DAOFilm("root", "1234", "127.0.0.1", "3306", "starwars");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String idStr = request.getParameter("id");
		
		try {
			
			int id = Integer.parseInt(idStr);
			daoFilm.supprimer(id);
			
		} catch (NumberFormatException e) {
			response.sendRedirect("/starwars/error");
		}
		
		response.sendRedirect("/starwars/films");
	}
}
