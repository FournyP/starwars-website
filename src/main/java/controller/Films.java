package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DAOFilm;
import modeles.Film;

/**
 * Servlet implementation class Films
 */
@WebServlet(urlPatterns = {"/films"})
public class Films extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private DAOFilm daoFilm;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Films() {
        super();
        
        daoFilm = new DAOFilm("root", "1234", "127.0.0.1", "3306", "starwars");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Film> films = daoFilm.listerFilms();

		request.setAttribute("films", films);
		
		request.getRequestDispatcher("/films.jsp").forward(request, response);
	}
}
